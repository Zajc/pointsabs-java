package pl.sdacademy.day14.Points;


import java.util.*;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        calculate();
    }

    private static void calculate() {
        List<Point> points = sort(colektPoint(setPointNumber()));
        points.forEach(point -> System.out.println(point.getName() + ": x = " + point.getX() + ", y = " + point.getY() + ", ABS = " + point.getAbs()));
    }

    private static List<Point> sort(List<Point> point) {
        if (point == null || point.size() == 0) {
            return null;
        }
        final Comparator<Point> pointsComparator = (o1, o2) -> (int) (Math.round(o1.getAbs()) - Math.round(o2.getAbs()));
        point.sort(pointsComparator);
        return point;
    }

    private static List<Point> colektPoint(int a) {
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            Point point1 = new Point(setName(), setCoordinate(), setCoordinate());
            points.add(point1);
        }
        return points;
    }

    private static int setPointNumber() {
        System.out.println("Enter point number: ");
        return scanner.nextInt();
    }

    private static String setName() {
        System.out.println("Enter name: ");
        return scanner.next();
    }

    private static double setCoordinate() {
        System.out.println("Enter coordinate: ");
        return scanner.nextDouble();
    }
}
